package com.alfabattle.sokian;

import com.alfabattle.sokian.infra.handlers.HealthHandler;
import com.alfabattle.sokian.infra.handlers.HelloHandler;
import com.alfabattle.sokian.infra.handlers.RatpackAlertErrorHandler;
import com.alfabattle.sokian.infra.handlers.RatpackClientErrorHandler;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ratpack.error.ClientErrorHandler;
import ratpack.error.ServerErrorHandler;
import ratpack.func.Action;
import ratpack.handling.Chain;
import ratpack.handling.RequestLogger;
import ratpack.registry.Registry;
import ratpack.server.ServerConfig;
import ratpack.spring.config.EnableRatpack;

import java.io.File;
import java.security.Security;

@SpringBootApplication(
        scanBasePackages = {
                "com.alfabattle.sokian"
        }
)
@EnableRatpack
public class TestApp {
    static {
        Security.setProperty("networkaddress.cache.ttl" , "60");
    }

    @Configuration
    public static class Config {
        public static final int THREADS = 32;
        private static final ServerErrorHandler serverErrorHandler = new RatpackAlertErrorHandler();
        private static final ClientErrorHandler clientErrorHandler = new RatpackClientErrorHandler();

        @Bean
        ServerConfig serverConfig() {
            return ServerConfig.builder()
                    .threads(THREADS)
                    .baseDir(new File("").getAbsoluteFile())
                    .port(2020)
                    .build();
        }

        @Bean
        Action<Chain> chain() {
            return chain -> chain
                    .all(ctx -> ctx.next(Registry.single(serverErrorHandler)))
                    .all(ctx -> ctx.next(Registry.single(clientErrorHandler)))
                    .all(configureRequestLogger())

                    .get("health", new HealthHandler())
                    .get("hello", new HelloHandler())
                    ;
        }

        @Bean
        public ObjectMapper objectMapper() {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
            return objectMapper;
        }

        private static RequestLogger configureRequestLogger() {
            return RequestLogger.of(requestOutcome -> {
                long durationMs = 1000 * requestOutcome.getDuration().getSeconds() + requestOutcome.getDuration().getNano() / 1000000;
                RequestLogger.LOGGER.info("[{} ms] {} {}",
                        durationMs,
                        requestOutcome.getRequest().getMethod(),
                        requestOutcome.getRequest().getUri()
                );
            });
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(TestApp.class, args);
    }
}
