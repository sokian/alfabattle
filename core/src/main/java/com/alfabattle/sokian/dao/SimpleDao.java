package com.alfabattle.sokian.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class SimpleDao {
    private static final Logger logger = LoggerFactory.getLogger(SimpleDao.class);
    @PostConstruct
    public void start() {
        logger.info("starting simple dao");
    }
}
