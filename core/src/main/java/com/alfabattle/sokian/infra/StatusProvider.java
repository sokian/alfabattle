package com.alfabattle.sokian.infra;

import org.springframework.stereotype.Component;

@Component
public class StatusProvider {
    public enum Status {
        READY,
        NOT_READY,
        ;
    }

    private volatile Status status = Status.READY;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
