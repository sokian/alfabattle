package com.alfabattle.sokian.infra.handlers;

import com.alfabattle.sokian.infra.StatusProvider;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.jackson.Jackson;
import ratpack.util.MultiValueMap;

import java.util.Collections;


public class HealthHandler implements Handler {
    private static final Logger logger = LoggerFactory.getLogger(HealthHandler.class);
    @Override
    public void handle(Context ctx) throws Exception {
        StatusProvider statusProvider = ctx.get(StatusProvider.class);

        MultiValueMap<String, String> queryParams = ctx.getRequest().getQueryParams();
        String status = queryParams.get("status");

        if (status == null) {
            if (statusProvider.getStatus() == StatusProvider.Status.READY) {
                ctx.render(Jackson.json(Collections.emptyList()));
            } else {
                ctx.clientError(HttpStatus.SC_SERVICE_UNAVAILABLE);
            }
        } else {
            StatusProvider.Status value = StatusProvider.Status.valueOf(status);
            statusProvider.setStatus(value);
            logger.info("Set new status " + value);
            ctx.render(Jackson.json(Collections.emptyList()));
        }
    }
}
