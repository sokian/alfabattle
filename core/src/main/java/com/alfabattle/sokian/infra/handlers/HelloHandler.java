package com.alfabattle.sokian.infra.handlers;

import ratpack.exec.Blocking;
import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.jackson.Jackson;

public class HelloHandler implements Handler {
    @Override
    public void handle(Context ctx) throws Exception {
        Blocking
                .get(() -> new SimpleHelloClass("you", "hello"))
                .then(response -> ctx.render(Jackson.json(response)));
    }
}
