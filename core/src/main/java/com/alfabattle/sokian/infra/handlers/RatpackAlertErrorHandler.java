package com.alfabattle.sokian.infra.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ratpack.error.ServerErrorHandler;
import ratpack.handling.Context;

public class RatpackAlertErrorHandler implements ServerErrorHandler {
    private static final Logger logger = LoggerFactory.getLogger(RatpackAlertErrorHandler.class);

    @Override
    public void error(Context context, Throwable throwable) throws Exception {
        logger.error("internal error", throwable);
    }
}
