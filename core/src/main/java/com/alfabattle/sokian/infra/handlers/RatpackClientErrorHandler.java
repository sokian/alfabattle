package com.alfabattle.sokian.infra.handlers;

import ratpack.error.ClientErrorHandler;
import ratpack.handling.Context;

public class RatpackClientErrorHandler implements ClientErrorHandler {
    @Override
    public void error(Context context, int statusCode) throws Exception {
        context.getResponse().status(statusCode).send();
    }
}
