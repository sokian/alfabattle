package com.alfabattle.sokian.infra.handlers;

public class SimpleHelloClass {
    private final String name;
    private final String greeting;

    public SimpleHelloClass(String name, String greeting) {
        this.name = name;
        this.greeting = greeting;
    }

    public String getName() {
        return name;
    }

    public String getGreeting() {
        return greeting;
    }
}
