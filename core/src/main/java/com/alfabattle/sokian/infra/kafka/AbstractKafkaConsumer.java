package com.alfabattle.sokian.infra.kafka;

import com.alfabattle.sokian.utils.ThreadUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Named;
import java.time.Duration;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.util.concurrent.TimeUnit.SECONDS;


public abstract class AbstractKafkaConsumer<E> {
    private final static Logger logger = LoggerFactory.getLogger(AbstractKafkaConsumer.class);
    public static final int DEFAULT_NUMBER_OF_WORKERS = 5;

    @Inject
    protected ObjectMapper objectMapper;

    @Inject
    @Value("${alfabattle.kafka.servers}")
    @Named("alfabattle.kafka.servers")
    private String kafkaServers;

    private final AtomicBoolean isRunning = new AtomicBoolean();
    private ExecutorService workerExecutorService;

    @PostConstruct
    public void init() {
        start();
    }

    @PreDestroy
    public void stop() {
        isRunning.set(false);
        workerExecutorService.shutdown();
    }

    private void start() {
        workerExecutorService = new ThreadPoolExecutor(
                5 + getNumberOfWorkers(),
                10 + getNumberOfWorkers() * 10,
                100, SECONDS,
                new ArrayBlockingQueue<>(1000),
                new CustomizableThreadFactory("Aggregator-" + getAggregatorName())
        );
        isRunning.set(true);
        new Thread("ConsumerLoop-" + getAggregatorName()) {
            @Override
            public void run() {
                runConsumerLoop();
            }
        }.start();
    }

    private void runConsumerLoop() {
        Properties properties = new Properties();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, getKafkaServers());
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, getAggregatorName() + "-aggregator-alfa");
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
        properties.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, 60*1000);
        properties.put(ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG, 90*1000);
        properties.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 2000);
        properties.put(ConsumerConfig.RECEIVE_BUFFER_CONFIG, 16 * 1024 * 1024);
        properties.put(ConsumerConfig.MAX_PARTITION_FETCH_BYTES_CONFIG, 16 * 1024 * 1024);

        long timeoutMs = 500;
        try (KafkaConsumer<String, String> kafkaConsumer = new KafkaConsumer<>(properties)) {
            kafkaConsumer.subscribe(getTopics());
            while (isRunning.get()) {
                try {
                    ConsumerRecords<String, String> records = kafkaConsumer.poll(Duration.ofMillis(timeoutMs));
                    if (records.count() != 0) {
                        while (true) {
                            try {
                                workerExecutorService.submit(() -> {
                                    logger.debug("Received " + records.count() + " records from kafka");
                                    List<E> events = parseEvents(records);
                                    if (!events.isEmpty()) {
                                        logger.debug("Processing " + events.size() + " events");
                                        processEvents(events);
                                    }
                                });
                                break;
                            } catch (RejectedExecutionException e) {
                                logger.warn("Task rejected, retrying");
                                ThreadUtils.sleepIgnoringInterrupts(timeoutMs);
                            }
                        }
                    }
                    ThreadUtils.sleepIgnoringInterrupts(Duration.ofMillis(5));
                } catch (Exception e) {
                    logger.error("Error in consumer loop", e);
                }
            }
        }
    }

    protected String getKafkaServers() {
        return kafkaServers;
    }
    protected abstract void processEvents(List<E> events);
    protected abstract List<E> parseEvents(ConsumerRecords<String, String> records);
    protected abstract String getAggregatorName();
    protected abstract List<String> getTopics();

    protected int getNumberOfWorkers() {
        return DEFAULT_NUMBER_OF_WORKERS;
    }
}
