package com.alfabattle.sokian.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;

public class ThreadUtils {
    private ThreadUtils() {

    }
    private final static Logger logger = LoggerFactory.getLogger(ThreadUtils.class);

    public static void sleepIgnoringInterrupts(Duration duration) {
        sleepIgnoringInterrupts(duration.toMillis());
    }

    public static void sleepIgnoringInterrupts(long millis) {
        long start = monotonicTimeMillis();
        long timeToSleep;
        while ((timeToSleep = millis - (monotonicTimeMillis() - start)) > 0) {
            try {
                //noinspection BusyWait
                Thread.sleep(timeToSleep);
            } catch (InterruptedException e) {
                logger.warn("interrupted while sleeping", e);
            }
        }
    }

    public static long monotonicTimeMillis() {
        return TimeSource.nanoTime() / 1000000;
    }
}
