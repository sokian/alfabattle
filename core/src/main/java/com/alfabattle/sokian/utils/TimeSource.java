package com.alfabattle.sokian.utils;

import com.github.benmanes.caffeine.cache.Ticker;

import java.time.Instant;
import java.util.Date;

public final class TimeSource {
    protected static TimeProvider timeProvider = new DefaultTimeProvider();

    public static long currentTimeMillis() {
        return timeProvider.currentTimeMillis();
    }

    public static long nanoTime() {
        return timeProvider.nanoTime();
    }

    public static Instant instantNow() {
        return timeProvider.instantNow();
    }

    public static Date dateNow() {
        return timeProvider.dateNow();
    }

    public static Ticker ticker() {
        return timeProvider.ticker();
    }

    public interface TimeProvider {
        default long currentTimeMillis() {
            return System.currentTimeMillis();
        }

        default long nanoTime() {
            return System.nanoTime();
        }

        default Instant instantNow() {
            return Instant.now();
        }

        default Date dateNow() {
            return new Date();
        }

        default Ticker ticker() {
            return this::nanoTime;
        }
    }

    private static class DefaultTimeProvider implements TimeProvider {}
}
