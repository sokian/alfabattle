#!/bin/bash

set -eux

DOCKER_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

MODULE_NAME="app"
TAG_NAME="0.1"

pushd "${DOCKER_DIR}/../../"
./gradlew clean
./gradlew bootJar -x test -p "${MODULE_NAME}"
mkdir -p ${DOCKER_DIR}/build/
cp ./${MODULE_NAME}/build/libs/*.jar ${DOCKER_DIR}/build/alfabattle.jar
popd

pushd "${DOCKER_DIR}"
docker build --tag alfabattle:${TAG_NAME} .
docker tag alfabattle:${TAG_NAME} sokian/alfabattle:${TAG_NAME}
docker push sokian/alfabattle:${TAG_NAME}
popd
