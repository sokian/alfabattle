#!/bin/bash

exec java -showversion -server \
    -Xms4G -Xmx4G -Xmn256M \
    -XX:+UnlockExperimentalVMOptions \
    -XX:+UseConcMarkSweepGC \
    -XX:+UseCMSInitiatingOccupancyOnly \
    -XX:CMSInitiatingOccupancyFraction=90 \
    -XX:+UseGCLogFileRotation \
    -XX:NumberOfGCLogFiles=10 \
    -XX:GCLogFileSize=10M \
    -XX:+ExitOnOutOfMemoryError \
    -XX:+PrintGC \
    -XX:+PrintGCDetails \
    -XX:+PrintGCDateStamps \
    -Xloggc:/var/opt/alfabattle/log/alfabattle.gc.log \
    -Dspring.profiles.active=production \
    -Dspring.config.name=application \
    -Dlogging.file=/var/opt/alfabattle/log/alfabattle.log \
    -Dio.grpc.internal.DnsNameResolverProvider.enable_grpclb=true \
    ${JVMARGS} -jar "/opt/alfabattle/lib/alfabattle.jar"
